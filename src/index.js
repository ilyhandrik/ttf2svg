const ttf2svg = require('../lib/ttf2svg');


const input = document.querySelector('#input-file');
const reader = new FileReader();

function fileInputSelector() {
    console.log(this.files[0]);
    reader.onload = () => {
        const buffer = reader.result;
        const svg = ttf2svg(buffer);
        console.log(svg);
    };
    reader.readAsArrayBuffer(this.files[0]);
}

input.addEventListener('change', fileInputSelector);
